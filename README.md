The details of this bug can be seen in this video: https://www.youtube.com/watch?v=DD-cUaDTBFA&feature=youtu.be 

We have investigated and found a bug in Linux (Unconfirmed) Open Fortress servers that cause teleporters not to keep your movement momentum upon traveling through a teleporter_trigger entity. 

Because of this, you can occasionally get stuck inside of something if there's an item spawn or something else too close to a teleporter exit. It places you into a state where you appear to be hovering on air as if it were a solid surface.

Dying while in this state causes the server to freak out with an "Excessive Sizelevel" error. When this happens, the server loses collision with objects like dynamite, ammo boxes, and so on. The server reports having to clean up these falling entities with a NaN Z axis. 

If enough of these entities all fall at once, as we demonstrate by killing a wave of bots, it can cause a server crash, which is easily reproducible with a large group of players naturally.

We have also been able to fix this using sourcemod, and a hand written plugin that carries the momentum of a player through their teleporter trip.

# How to use
***sm_teleport_toggle*** 1/0 - Enables the plugin