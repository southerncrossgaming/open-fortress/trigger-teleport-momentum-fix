#include <sdktools>

#define PLUGIN_VERSION "1.0.0"
public Plugin myinfo = {
    name = "Trigger Teleport Momentum",
    author = "Rowedahelicon",
    description = "Fixes a player's movement externally on teleport.",
    version = PLUGIN_VERSION,
    url = "rowedahelicon.com"
};

ConVar enabled;
float g_vecPlayerVelocity[MAXPLAYERS+1][3];

public void OnPluginStart() {
	enabled = CreateConVar("sm_teleport_toggle", "1", "Adjust a player's movement externally on teleport.");
	HookEntityOutput("trigger_teleport", "OnStartTouch", TeleTriggerIn);
	HookEntityOutput("trigger_teleport", "OnEndTouch", TeleTriggerOut);
}

public void TeleTriggerIn(const char[] output, int caller, int activator, float delay) {

	int target = GetTeleportExit(caller);
	if (target == -1)
		return;
	
	float m_vecOrigin[3]; float m_angRotation[3]; float m_vecAbsVelocity[3];
	GetEntPropVector(target, Prop_Send, "m_vecOrigin", m_vecOrigin);
	GetEntPropVector(target, Prop_Send, "m_angRotation", m_angRotation);
	GetEntPropVector(activator, Prop_Data, "m_vecAbsVelocity", m_vecAbsVelocity);

	m_vecAbsVelocity[2] = 0.0;
	
	float speed = GetVectorLength(m_vecAbsVelocity);
	float p_vec[3];
	GetAngleVectors(m_angRotation, p_vec, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(p_vec, p_vec);
	ScaleVector(p_vec, speed);
	g_vecPlayerVelocity[activator] = p_vec;
}

public void TeleTriggerOut(const char[] output, int caller, int activator, float delay) {
	if(activator > 0 && activator <= MaxClients && IsPlayerAlive(activator) && enabled.BoolValue) {
		TeleportEntity(activator, NULL_VECTOR, NULL_VECTOR, g_vecPlayerVelocity[activator]); 
	}
}

stock int GetTeleportExit(int trigger_teleport)
{
	char m_target[128]; char m_target_name[128]; int target; int f_target;
	GetEntPropString(trigger_teleport, Prop_Data, "m_target", m_target, sizeof(m_target));
	
	//This is because some maps use one or the other -w-
	while ((target = FindEntityByClassname(target, "info_teleport_destination")) != -1)
    {
        GetEntPropString(target, Prop_Data, "m_iName", m_target_name, sizeof(m_target_name));
        if (StrEqual(m_target_name, m_target))
        {
			f_target = target;
			break;
        }
    }
	while ((target = FindEntityByClassname(target, "point_teleport")) != -1)
    {
        GetEntPropString(target, Prop_Data, "m_iName", m_target_name, sizeof(m_target_name));
        if (StrEqual(m_target_name, m_target))
        {
			f_target = target;
			break;
        }
    } 
	return f_target;
}